describe('PieController', function () {
    var $rootScope, $scope, controller;

    beforeEach(function () {
        module('pie');

        inject(function ($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            controller = $injector.get('$controller')('PieController', {$scope: $scope});
        });
    });

    describe("Action Handlers", function () {
        describe("eatSlice", function () {
            it("Should decrement the number of slices", function () {
                expect($scope.slices).toEqual(8);
                $scope.eatSlice();
                expect($scope.slices).toEqual(7);
            });

            it("Should do nothing when the number of slices is 0", function () {
                $scope.slices = 0;
                $scope.eatSlice();
                expect($scope.slices).toEqual(0);
            });
        });

        describe("requestFlavor", function () {
            it("Should set $scope.lastRequestFlavor to the passed in argument", function () {
                controller.requestFlavor("Cherry");
                expect($scope.lastRequestedFlavor).toEqual("Cherry");
            });
        });
    });

    describe("Initialization", function () {
        it("Should instantiate the slices to 8", function () {
            expect($scope.slices).toEqual(8);
        });

        it("Should instantiate $scope.lastRequestedFlavor", function () {
            expect($scope.lastRequestedFlavor).toBeUndefined();
        });
    });

    describe("Listeners", function () {
        it("Should set the warning to RED ALERT", function () {
            $rootScope.$broadcast('event');
            $scope.$digest();
            expect($scope.warning).toEqual('RED ALERT');
        });

        it("Should set the slices to 0", function () {
            $rootScope.$broadcast('event');
            $scope.$digest();
            expect($scope.slices).toEqual(0);
        });
    });
});