describe("titleCase", function () {
    var $filter, filter;

    beforeEach(function () {
        module('filters');
        inject(function ($injector) {
            $filter = $injector.get('$filter');
            filter = $filter('titleCase');
        });
    });

    it("Should return undefined when undefined is passed in", function () {
        expect(filter(undefined)).toBeUndefined();
    });

    it("Should return null when null is passed in", function () {
        expect(filter(null)).toBeNull();
    });

    it("Should return a blank string when a blank string is passed in", function () {
        expect(filter("")).toEqual("");
    });

    it("Should change the casing of a lower cased word", function () {
       expect(filter("testing filter")).toEqual("Testing Filter");
    });

    it("Should change the casing of a upper cased word", function () {
        expect(filter("TESTING FILTER")).toEqual("Testing Filter");
    });

    it("Should change the casing of a random", function () {
        expect(filter("TeSTing FILter")).toEqual("Testing Filter");
    });

    it("Should play nice with a normal phrase", function () {
        expect(filter("Testing Filter")).toEqual("Testing Filter");
    });
});