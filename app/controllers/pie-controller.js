angular.module('pie', []).controller('PieController', ['$scope', function ($scope) {
    $scope.eatSlice = function () {
        if ($scope.slices) {
            $scope.slices--;
        }
    };

    $scope.$on('event', function () {
        $scope.warning = 'RED ALERT';
        $scope.slices = 0;
    });

    this.requestFlavor = function (flavor) {
        $scope.lastRequestedFlavor = flavor;
    };

    $scope.lastRequestedFlavor;
    $scope.slices = 8;
}]);
